#!/usr/bin/env puma

app_path = '/home/deployer/sites/auth'

directory "#{app_path}/current"

rackup "#{app_path}/current/config.ru"

daemonize true
#port 9292
pidfile "#{app_path}/shared/tmp/pids/puma.pid"

state_path "#{app_path}/shared/tmp/pids/puma.state"

stdout_redirect "#{app_path}/shared/log/puma_stdout.log", "#{app_path}/shared/log/puma_stderr.log"

threads 6, 6

bind "unix:#{app_path}/shared/tmp/sockets/puma.sock"

workers 2

preload_app!

tag 'auth'
