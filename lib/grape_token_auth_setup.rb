require_relative 'models/user'
GrapeTokenAuth.configure do |config|
  config.smtp_configuration = {
    :port      => ENV['SMTP_PORT'],
    :address   => ENV['SMTP_ADDRESS'],
    :user_name => ENV['SMTP_USERNAME'],
    :password  => ENV['SMTP_PASSWORD']
  }
end

GrapeTokenAuth.setup! do |config|
  config.mappings = { user: User }
  config.secret = ENV['SECRET_TOKEN']
  config.from_address = ENV['APP_MAIL_FROM']
  config.default_url_options = { host: ENV["URL_HOST"], ssl: false }
end

class GrapeTokenAuth::Mail::ConfirmationEmail
  def confirmation_link
    protocol = url_options[:ssl] ? URI::HTTPS : URI::HTTP
    options = url_options.merge(path: "/auth/confirmation", query: confirmation_params.to_query)
    protocol.build(options).to_s
  end
end
