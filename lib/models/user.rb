require 'active_record'
Dotenv.load "./.env.#{ENV['RACK_ENV']}"

class User < ActiveRecord::Base
  establish_connection ENV['DATABASE_URL']
  include GrapeTokenAuth::ActiveRecord::TokenAuth
  #validates :operating_thetan, numericality: { greater_than: 0 }
  after_create :send_confirmation_mail

  def send_confirmation_mail
    self.send_confirmation_instructions({
      redirect_url: ENV['ORIGIN_URL'],
      client_config: "default", 
      confirmation_token: self.confirmation_token
    })
  end
end

