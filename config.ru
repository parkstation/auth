Bundler.setup
require "dotenv"
Dotenv.load Dir.pwd + ".env.#{ENV['RACK_ENV']}"
require 'rubygems'
require 'bundler'
require 'mail'
require 'rack/cors'

require 'warden'
require 'omniauth'
require 'omniauth-github'
require 'grape_token_auth'
require_relative 'lib/grape_token_auth_demo'

use Rack::Cors do
  allow do
    origins 'localhost:3000', 'localhost:2020', 'parkstation.pp.ua'
    resource '*', headers: :any, methods: :any,
    expose: ['access-token', 'expiry', 'token-type', 'uid', 'client']
  end
end

use Rack::Session::Cookie, secret: 'blah'

GrapeTokenAuth.setup_warden!(self)

run GrapeTokenAuthDemo

